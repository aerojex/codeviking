# Prostate Symptom Score calculation

def pss_param(arr):
    total = sum(arr)

    if total<=7 and total>=0:
        return "Mild"
    elif total>=8 and total<=19:
        return "Moderate"
    elif total>=20 and total<=35:
        return "Severe"
    else:
        return "Error"