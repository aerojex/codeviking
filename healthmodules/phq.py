# Patient health questionare

def phq_param(arr):
    total = sum(arr)

    if total<=4 and total>=0:
        return "None"
    elif total>=5 and total<=9:
        return "Mild"
    elif total>=10 and total<=14:
        return "Moderate"
    elif total>=15 and total<=19:
        return "Moderately Severe"
    elif total>=20 and total<=27:
        return "Severe"
    else:
        return "Error"