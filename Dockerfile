# this is my base image
FROM alpine:3.5

# Install python and pip
RUN apk add --update py2-pip

# install Python modules needed by the Python app
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# copy files required for the app to run
COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/
COPY main.py /usr/src/app/
COPY healthmodules /usr/src/app/healthmodules/
RUN ["chmod", "+x", "/usr/src/app/"]

# run the application
EXPOSE 5000
CMD ["python", "/usr/src/app/app.py"]
