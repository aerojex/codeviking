from healthmodules.pss import *
from healthmodules.phq import *
from healthmodules.ess import *
from healthmodules.bmi import *


class patient_info:
    def __init__(self,name,age,height,weight):
        self.name = name
        self.age = age
        self.height = height
        self.weight = weight

class pss_info:
    def __init__(self,param1,param2,param3,param4,param5,param6,param7):
        self.param1 = param1
        self.param2 = param2
        self.param3 = param3
        self.param4 = param4
        self.param5 = param5
        self.param6 = param6
        self.param7 = param7

    def evaluate_pss(self):
        arr = [self.param1,self.param2,self.param3,self.param4,self.param5,self.param6,self.param7]
        remark = pss_param(arr)
        return remark

class phq_info:
    def __init__(self,param1,param2,param3,param4,param5,param6,param7,param8,param9):
        self.param1 = param1
        self.param2 = param2
        self.param3 = param3
        self.param4 = param4
        self.param5 = param5
        self.param6 = param6
        self.param7 = param7
        self.param8 = param8
        self.param9 = param9

    def evaluate_phq(self):
        arr = [self.param1,self.param2,self.param3,self.param4,self.param5,self.param6,self.param7,self.param8,self.param9]
        remark = phq_param(arr)
        return remark

class ess_info:
    def __init__(self,param1,param2,param3,param4,param5,param6,param7,param8):
        self.param1 = param1
        self.param2 = param2
        self.param3 = param3
        self.param4 = param4
        self.param5 = param5
        self.param6 = param6
        self.param7 = param7
        self.param8 = param8

    def evaluate_ess(self):
        arr = [self.param1,self.param2,self.param3,self.param4,self.param5,self.param6,self.param7,self.param8]
        remark = ess_param(arr)
        return remark


# def evaluate(name,age,height,weight):
#     h = float(height)
#     w = float(weight)

#     patient = patient_info(name,age,h,w)
#     bmi = get_bmi(patient.weight,patient.height)
#     bmi_remark = bmi_eval(bmi)
    
#     print(bmi_remark)


def main():
    name = input("Enter name: ")
    age = input("Enter age: ")
    height = float(input("Enter height in M: "))
    weight = float(input("Enter weight in Kg: "))

    patient = patient_info(name,age,height,weight)
    bmi = get_bmi(patient.weight,patient.height)
    bmi_remark = bmi_eval(bmi)
    
    print(bmi_remark)

if __name__ == "__main__":
    main()   
